// module to be imported as client library to help with sign in to dingsda v1 


/* 
TODO: 
- add @example to all function definition documentations
- add handover functions
- implement picture upload/update function to create and update functions
- add delete item function
*/

let loginSuccessEvent = new Event('dingsdaloginsuccess');
let loginErrorEvent = new Event('dingsdaloginerror');

/**
 * 
 * .APIURL will be the container inside of the module for the dingsda instance of our choice. 
 * It has to be a valid dingsda API v1 endpoint and will default to https://dingsda.org:3000/api/v1/
 * if not specified/changed via {@link setAPIUrl}
 * It can be read via {@link getAPIUrl}
 */
let APIURL = null;

/**
 * 
 * @param {string} apiURL - the Base/Root URL of dingsda server (e.g. https://dingsda.org:3000/api/v1/)
 */
function setAPIUrl(apiURL){
  if (!apiURL.endsWith("/")){apiURL = apiURL + "/";}
  APIURL = apiURL;
  console.log(APIURL);
}

/**
 * get the set apiURL of this dingsda module
 * @returns {string}
 */
function getAPIUrl(){
  return APIURL;
}

/**
 * will login and will fetch information about the user via pw and username and
 * if the browser allows it, will also set the needed Auth Cookie 
 * 
 * this is XHR because I hit some issues with the cookie setting in the otherwise 
 * prefered ES6 fetch API. Help wanted!
 * 
 * @param {string} username
 * @param {string} password
 * @param {function=} cb - callback (optional)
 * @param {string=} apiURL - URL of baseAPI to query. (optional if APIURL set already)
 * 
 * @returns {promise} Promise of login
 * 
 * @example
 * ///////////////////////////////////
 * /// 3 different ways to be used: //
 * ///////////////////////////////////
 
// login
dingsda.login(USERNAME,PASSWORD);

// Listen for the dingsdaloginsuccess event before you use the other dingsdaclient functions:
document.addEventListener('dingsdaloginsuccess', function (e) { 
    // do all the things...
}, false);

// Listen for the dinsdaloginerror event to catch login errors:
document.addEventListener('dingsdaloginerror', function (e) { 
    // catch your error
}, false);

////////////
/// or /////

dingsda.login(USERNAME,PASSWORD, function(res){ // success... do all the things};

////////////
/// or /////

dingsda.login(USERNAME,PASSWORD).then((res)=>{
    // login success via promise
    // do all the things...
})
*/
function login(username,password,cb=function(){},apiURL=APIURL){

  return new Promise((resolve,reject)=>{
    var data = null;

    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
          if (this.status === 200)
          {
            console.log("login success, cookie will be set for user", this);
            document.dispatchEvent(loginSuccessEvent); // dispatch event
            cb(this.responseText);
            resolve(this.responseText)
          }
          else
          {
            console.error("login error");
            document.dispatchEvent(loginErrorEvent); // dispatch event
            cb(this);
            reject(this)
          }
      }

    }
    )

    xhr.open("GET",`${apiURL}${username}?name=${username}&password=${password}`);
    xhr.setRequestHeader("cache-control", "no-cache","no-same-site");

    xhr.send(data);
  })

}

/**
 * 
 * @param {string} idOrURL - ID of item or URL to items database Entry
 * @param {string=} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {string=} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 * 
 * @description
 * fetches all data about one item from dingsda
 * 
 * @example
 * // 3 legal ways to use this method:
 *  dingsda.getItem("https://dingsda.org:3000/api/v1/<username>/<itemid>")
 * // or
 *  dingsda.setAPIUrl("https://dingsda.org:3000/api/v1/");
 *  dingsda.getItem("<username>/<itemid>");
 *  // or
 *  dingsda.setAPIUrl("https://dingsda.org:3000/api/v1/");
 *  dingsda.getItem("<itemid>","<username>");
 * 
 */
async function getItem(idOrURL,dbName="",apiURL=APIURL){ // TODO: add picture helper

  if (dbName && dbName!=""){dbName = dbName+"/"}

  if (!idOrURL.startsWith("https://") && !idOrURL.startsWith("http://"))
  {
    idOrURL = apiURL + dbName + idOrURL;
  }
  console.log(idOrURL);
  
  let res = await fetch(idOrURL,{credentials: "include"});

  if (!res.ok){
    if (res.status === 404){
      throw "dingsda item not found."
    }
    else {throw res}
  }

  let output = await res.json();

  if (!output._id){
    throw "invalid response. did you provide all info needed? maybe forgot dbName?"
  }

  return output;
}



/**
 * 
 * @param {string} idOrURL - ID of item or URL to items database Entry
 * @param {object} newlocation - the new location of the object in this form:
 *  {latitude <float>, longitude:<float>, address:<string optional>, description:<string optional>}
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function moveItem(idOrURL,newlocation={address:"", latitude:null, longitude:null, description:""},dbName="",apiURL=APIURL){

  if (dbName && dbName!=""){dbName = dbName+"/"}

  if (!idOrURL.startsWith("https://") && !idOrURL.startsWith("http://"))
  {
    idOrURL = apiURL + dbName + idOrURL;
  }

  // validate newlocation to validatedlocation
  let validatedlocation = {}

  if ((!newlocation.latitude || !newlocation.longitude) && !newlocation.insideOf ){
    throw "moveItem() needs at least .latitude and .longitude"
  }

  if ((typeof newlocation.latitude === "string" || typeof newlocation.latitude === "number") 
      &&  (typeof newlocation.longitude === "string" || typeof newlocation.longitude === "number")){
    validatedlocation.latitude = newlocation.latitude;
    validatedlocation.longitude = newlocation.longitude;
  }
  else if (typeof newlocation.insideOf === "string")
  {
    validatedlocation.insideOf = newlocation.insideOf
  }
  else
  {
    throw("moveItem(): .latitude and .longitude need to be either of type string or number")
  }

  if (newlocation.address && (typeof newlocation.address === "string")){
    validatedlocation.address = newlocation.address
  } // feature: get lat and long from address if not provided and possible?

  if (newlocation.description && (typeof newlocation.description === "string")){
    validatedlocation.description = newlocation.description
  } // feature: get lat and long from address if not provided and possible?


  let res = await fetch(idOrURL,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"move",
          "location":validatedlocation
        }]
      }
    )
    
  })
  
  if (!res.ok){
    if (res.status === 404){
      throw "dingsda item not found."
    }
    else {throw res}
  }

  return await res.json();

}


/**
 * 
 * updates dingsda item defined by idORURL with updates defined in itemChanges object.
 * 
 * DANGER: this function will automatically fetch the object defined via idORURL and merge the itemChanges object
 * into the original object for an update. No _rev field will be necessary to check if you are updating the correct version
 * Do not use in environments where more than one person/client might be editing objects at the same time!
 * 
 * In those environments rather use {@link updateItemSafe}
 * 
 * @param {string} idOrURL - ID of item or URL to items database Entry
 * @param {object} itemChanges - dingsda objects changes. this should include all fields and subfields 
 * that will be overwritten in the original item
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 * 
 * @example 
dingsda.updateItemSimple("08641a09-4a17-5f18-8e91-db3168a40a50",{name:"Samsung S2 Tablet Android with pen"},"jan")
 */
async function updateItemSimple(idOrURL,itemChanges,dbName,apiURL=APIURL){ // todo: add pic update helper

  if (itemChanges.hyperlink){return console.error("items hyperlink cannot be changed")}
  if (itemChanges._id){return console.error("items hyperlink cannot be changed")}
  if (itemChanges._rev){return console.error("items _rev is not needed in UpdateItemSimple(). \
                                              If you want to be safe use updateItemSafe")}
  // get item URL from data provided
  if (dbName && dbName!=""){dbName = dbName+"/"}

  if (!idOrURL.startsWith("https://") && !idOrURL.startsWith("http://"))
  {
    idOrURL = apiURL + dbName + idOrURL;
  }

  let oldItem = await getItem(idOrURL)
  .catch((err)=>{return console.error("error fetching original item",err);});
  let newItem = Object.assign(oldItem,itemChanges);

  validateItem(newItem);

  let res = await fetch(idOrURL,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"update",
          "doc":newItem
        }]
      }
    )
  })


  return res

}

/**
 * update item safe
 * this is for data save itemChanges. For that you need the original item, edit the object
 * and pass it into this function, including its _rev param
 * 
 * it will overwrite the full item with itemChanged, if _rev is the most recent.
 * 
 * No URL or user database is needed for the item includes its own 
 * 
 * @param {object} itemChanged - full changed item including _rev
 * 
 * @example 
dingsda.getItem("08641a09-4a17-5f18-8e91-db3168a40a50","jan")
.then(async (res)=>{
  let item = res
  item.name = "Samsung S2 Tablet Android with pen";
  let upd = await dingsda.updateItem( item,"jan" )
  console.log(upd);
})
 */
async function updateItemSafe(itemChanged){ // todo: add pic update helper

  let URL = itemChanged.hyperlink;

  let res = await fetch(URL,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"update",
          "doc":itemChanged
        }]
      }
    )
  })

  return res
}


/**
 * will announce the handover of an item to another user and send this user a notification.
 * will also set up a handover-await-notification in sending users -notification database.
 * 
 * -notification database can be read out via {@link getNotifications} for further interactions with open handover announcements.
 * 
 * 
 * @param {string} idOrURL - id or URL of the item to be handed over
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) (in this case: the owner of the item who has to be authenticated)
 * @param {*} dbNameRecipient - name of the userDB of the recipient of the item handed over
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function handoverItem(idOrURL,recepientsUsername,dbName="",apiURL=APIURL){ 

  // get item URL from data provided
  if (dbName && dbName!=""){dbName = dbName+"/"}

  if (!idOrURL.startsWith("https://") && !idOrURL.startsWith("http://"))
  {
    idOrURL = apiURL + dbName + idOrURL;
  }

  if (idOrURL.endsWith("/")){ idOrURL = idOrURL.slice(0, -1);}

  let url = idOrURL;

  let res = await fetch(url,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"handover_announce",
          "username": recepientsUsername
        }]
      }
    )
  })

  return res
}

/**
 * get all Notifications from users -notifications database.
 * 
 * notifications can either be of type info-, handover-await, or handover-confirm
 * 
 * handover-await notifications can be cancelled with {@link cancelHandover}
 * handover-confirm notifications can be denied with {@link denyHandover} or confirmed with {@link confirmHandover}
 * handover-info notifications can be deleted with {@link deleteNotification}
 * 
 * @param {string} dbName 
 * @param {string} apiURL 
 */
async function getNotifications(dbName,apiURL=APIURL){

  let url = apiURL + dbName

  let res = await fetch(url,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"getNotifications"
        }]
      }
    )
  })
  console.log(res);
  
  let output = await res.json();
  
  return output
}

/**
 * deletes a notification from users -notification database.
 * 
 * usually only to be used to clear info notifications afer sb denied our handover. 
 * all other notifications can but should normally not be deleted through this method 
 * but should rather be deleted via {@link cancelHandover},{@link denyHandover}, or {@link confirmHandover}
 * 
 * @param {string} notificationID - _id of notification document. can be retrieved via {@link getNotifications}
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function deleteNotification(notificationID,dbName="",apiURL=APIURL){

    let url = apiURL + dbName
  
    let res = await fetch(url,{
      credentials: "include", 
      method:'POST',
      headers:{ 
        'Accept':'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(
        {
          "data":[{
            "type":"deleteNotification",
            "_id":notificationID
          }]
        }
      )
    })
    
    let output = await res.json();
    
    return output
  
}

/**
 * will confirm a {@link handover} that another user send us.
 * To be used when we received an item physically. 
 * 
 * @param {string} notificationIDorRef 
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {*} dbNameLender - name of the userDB of the lender of the item
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function confirmHandover(notificationIDorRef,dbName="",dbNameLender,apiURL=APIURL){

  notificationIDorRef = notificationIDorRef.replace("handover_confirm-","");
  //console.log("notication Ref is", notificationIDorRef );
  
  let url = apiURL + dbName;

  let res = await fetch(url,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"handover",
          "ref":notificationIDorRef,
          "from": dbNameLender
        }]
      }
    )
  })
  
  let output = res
  
  return output

}

/**
 * will deny a handover {@link handover} that another user send us.
 * To be used for example when we could not pick up the item or item handover was otherwise falsely announced.
 * 
 * @param {string} notificationIDorRef 
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {*} dbNameLender - name of the userDB of the lender of the item to be denied
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function denyHandover(notificationIDorRef,dbName="",dbNameLender,apiURL=APIURL){

  notificationIDorRef = notificationIDorRef.replace("handover_confirm-","");
  console.log("notication Ref is", notificationIDorRef );
  
  let url = apiURL + dbName;

  let res = await fetch(url,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"handover_deny",
          "ref":notificationIDorRef,
          "from": dbNameLender
        }]
      }
    )
  })
  //console.log(res);
  
  let output = res
  //console.log(output);
  
  return output
}

/**
 * will cancel a handover {@link handover} that we (the user) ourselves set up.
 * To be used for example when item could never handed over or rejected by recipient but announcement was posted.
 * 
 * @param {string} notificationIDorRef 
 * @param {*} dbName - name of the userDB to be queried (optional if in idOrURL) (in this case: the userDB of the user who posted the handover_announce) 
 * @param {*} dbNameRecipient - name of the userDB of the recipient of the item handed over
 * @param {*} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set) 
 */
async function cancelHandover(notificationIDorRef,dbName="",dbNameRecipient,apiURL=APIURL){

  notificationIDorRef = notificationIDorRef.replace("handover_await-","");
  console.log("notication Ref is", notificationIDorRef );
  
  let url = apiURL + dbName;

  let res = await fetch(url,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"handover_cancel",
          "ref":notificationIDorRef,
          "to": dbNameRecipient
        }]
      }
    )
  })
  //console.log(res);
  
  let output = res
  console.log(output);
  
  return output
}

/**
 * 
 * @param {string} idOrURL - ID of item or URL to items database Entry
 * @param {string} base64img - base64 encoded jpeg image
 * @param {boolean=false} overwrite - overwrite existing image flag
 * @param {string} dbName - name of the userDB to be queried (optional if in idOrURL) 
 * @param {string} apiURL - URL of the API base URL to be queried (optional if this.APIURL has been set)  
 * 
 * uploads and overwrites item image to an object. every object can only have one small_pic image assigned to it.
 * 
 */
async function uploadItemImage(idOrURL,base64img,overwrite=false,dbName,apiURL=APIURL){
  
  if (!idOrURL || !base64img){throw "not enough data provided to uploadItemImage"}

  // get item URL from data provided
  if (dbName && dbName!=""){dbName = dbName+"/"}

  if (!idOrURL.startsWith("https://") && !idOrURL.startsWith("http://"))
  {
    idOrURL = apiURL + dbName + idOrURL;
  }

  // check if item exists first:
  let item = await getItem(idOrURL);
  if (!item){ throw "item does not exist. please create item before uploading pictures to it" }
  if (!overwrite && item._attachments && item._attachments["pic_small.jpg"] && item._attachments["pic_small.jpg"] !== ""){
      console.error("item already has picture and overwrite flag is set to false. will not upload");
      return null
  }
  
  console.log("sending update to",idOrURL, item);
  
  let res = await fetch(idOrURL,{
    credentials: "include", 
    method:'POST',
    headers:{ 
      'Accept':'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(
      {
        "data":[{
          "type":"update",
          "doc":item,
          "pic":base64img
        }]
      }
    )
  })
  
  return res
}



function validateItem(item){

  if (!item._id){
    throw "item needs to have _id"
  }
  if (!item.location){
    throw "item needs to have location"
  }
  if (typeof item.location !== "object" || 
      !item.location.latitude || 
      !item.location.longitude
    ){
    throw "item location invalid"
  }
   // todo: check if needed serverside as well:
  if ( 
        !Object.keys(item).every((key) => {
          let isAnyOftheKeys = (key === "_id" || key === "name" || key === "owners"||  key === "location" || 
                                key === "other" || key === "_rev" || key === "created" || key === "hyperlink" || 
                                key === "lastedited" || key === "lasteditedBy" || key === "inPossessionOf" || 
                                key === "_attachments");
          console.log(key, isAnyOftheKeys);
          return isAnyOftheKeys
        })
  ){
    throw "item contains illegal fields"
  }
  if ( 
    !Object.keys(item.location).every((key) => {
      let isAnyOftheKeys = (key === "latitude" || key === "longitude" || key === "address" || key === "description")
      console.log(key, isAnyOftheKeys);
      return isAnyOftheKeys
    })
  ){
  throw "item location contains illegal fields"
  }
  
  
}



export{
  login,
  setAPIUrl,
  getAPIUrl,
  getItem,
  moveItem,
  updateItemSafe,
  updateItemSimple,
  handoverItem,
  getNotifications,
  deleteNotification,
  confirmHandover,
  denyHandover,
  cancelHandover,
  uploadItemImage,
  validateItem
}