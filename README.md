# dingsda javascript client library

This is a quick n dirty ES6 module intended to be used as client library to build Apps and Webpages communication with dingsda.org or other dingsda v1 APIs.

## usage

to use this in your javascript code, import `dingsda_client.js` it as <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules">ES6 module</a> in your javascript code:

```js

import * as dingsda from './dingsda_client.js'

```

after that you can use all methods described in the <a href="https://gitlab.com/gauguerilla/dingsda-client-lib/-/tree/master/out">documentation</a>